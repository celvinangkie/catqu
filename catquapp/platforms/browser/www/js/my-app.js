var $$ = Dom7;
var app = new Framework7({
	root: '#app',
	name: 'F7App',
	id: 'com.ubaya.f7app',
	panel: { swipe: 'left' },
	theme: 'md',
	routes: [
		{
			path: '/about/',
			url: 'about.html',
		}
	]
});

var mainView = app.views.create('.view-main');

