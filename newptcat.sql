-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27 Mar 2020 pada 03.37
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newptcat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_order`
--

CREATE TABLE `detail_order` (
  `id_user` int(10) NOT NULL,
  `id_order` varchar(100) NOT NULL,
  `id_produk` int(4) NOT NULL,
  `tanggal` date NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `status_order` varchar(100) DEFAULT NULL,
  `status_notif` varchar(20) NOT NULL DEFAULT 'belum'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_order`
--

INSERT INTO `detail_order` (`id_user`, `id_order`, `id_produk`, `tanggal`, `qty`, `subtotal`, `id`, `status_order`, `status_notif`) VALUES
(10, '5dfb92fc90006', 20, '2019-12-19', 5, 4500000, 1, 'Order Dibatalkan', 'read'),
(10, '5dfbcfa556d20', 21, '2019-12-19', 3, 36, 2, 'Order Dibatalkan', 'read'),
(10, '5dfbcfcfc0c2a', 21, '2019-12-19', 2, 24, 3, 'Order Dibatalkan', 'read'),
(15, '5e67e6da29962', 20, '2020-03-10', 1, 900000, 4, 'Sudah diproses', 'read');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id` int(11) NOT NULL,
  `gambar` varchar(1000) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id`, `gambar`, `id_produk`) VALUES
(9, 'XeatwmVf9hQAwnM4tgkhEEz6LR422PMbMZQmqA5X.jpg', 20),
(10, 'everglo-new_1496776326.jpg', 21),
(11, 'supersilk-new_1496776421.jpg', 22),
(12, 'no_odor_1496776352.jpg', 23),
(13, 'aquamatt-new_1496776300.jpg', 24),
(14, 'no-odor-medicare_1561955292.jpg', 25),
(15, 'supersilk-anti-noda_1561955668.jpg', 26),
(16, 'platinum_1467096142.jpg', 27),
(17, '6hVTSEpgFPABW3jkXdrM3weGcXbXmFdVG981V6GF.jpeg', 28),
(18, 'PMkslUP8AFIbQ3f9JwqjXOHtXiJc7sNuLykkczz8.jpeg', 29),
(19, 'RP9Ni7dwwC8wkKEUCuCCoqtuGlITOBUBqmK6xAtb.jpeg', 30),
(20, 'xwwAbiGDa8m40fzoKxDYpbn1lR9DDjWaf5t3lq3Y.jpeg', 31),
(21, 'IhpilGZutggbXezLX8JrcA68ed26EcQmkKL8cMRO.jpeg', 32),
(22, 'L83QpOROQFmc25nMMIFAXBNRbwUUyK9aX9inAepQ.jpeg', 33),
(23, 'RqBuJrLBzmmPludVVSq6bcveyEUWn53WtGqpXWUb.jpeg', 34),
(24, 'nYbKKCzvdU5DTiNTkUh4PaY4hlxA4hpEyoVYMKtF.jpeg', 35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Cat Tembok Premium'),
(2, 'Cat Kayu dan Besi'),
(3, 'Cat Pelapis Anti Bocor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `keranjang`
--

CREATE TABLE `keranjang` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notabeli`
--

CREATE TABLE `notabeli` (
  `idnota` int(4) NOT NULL,
  `id_user` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `grandtotal` double NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'belum lunas',
  `bukti_bayar` varchar(2000) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `catatan` varchar(1000) DEFAULT NULL,
  `due_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `notabeli`
--

INSERT INTO `notabeli` (`idnota`, `id_user`, `tanggal`, `grandtotal`, `status`, `bukti_bayar`, `alamat`, `catatan`, `due_date`) VALUES
(29, 10, '2019-12-19', 900000, 'barang diambil', 'fjuBXx26sLfwvSEUajaaOndmdUmb3RgaFPucxiCL.png', 'asdsad', 'asdsad', '2019-12-21'),
(30, 15, '2020-03-10', 2500000, 'barang diambil', 'vnuPxSSLy4nTdwxrK1O5Z1dobqHptM6TiO76dpm6.jpeg', NULL, NULL, '2020-03-12'),
(31, 15, '2020-03-10', 900000, 'menunggu pengambilan', 'fynUrpI9DtYHDr46CHV1JBAB7yvb0oDdCz8CEJCQ.png', 'run', 'gkewfasd', '2020-03-12'),
(32, 15, '2020-03-10', 900000, 'belum lunas', NULL, NULL, NULL, '2020-03-12'),
(33, 15, '2020-03-10', 10000, 'belum lunas', NULL, NULL, NULL, '2020-03-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nota_has_produk`
--

CREATE TABLE `nota_has_produk` (
  `id` int(11) NOT NULL,
  `id_produk` int(4) NOT NULL,
  `id_nota` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nota_has_produk`
--

INSERT INTO `nota_has_produk` (`id`, `id_produk`, `id_nota`, `qty`, `subtotal`) VALUES
(29, 20, 29, 1, 900000),
(30, 25, 30, 1, 2500000),
(31, 20, 31, 1, 900000),
(32, 20, 32, 1, 900000),
(33, 22, 33, 1, 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('adm@mail.com', '$2y$10$2bBS2OVcEgBuRgO1cwdK.u/3k68QvfpZhI32GTs8TxBUNbkAflQy.', '2019-08-18 00:22:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `idproduk` int(4) NOT NULL,
  `namabarang` varchar(100) NOT NULL,
  `harga` double NOT NULL,
  `deskripsi` text NOT NULL,
  `id_warna` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`idproduk`, `namabarang`, `harga`, `deskripsi`, `id_warna`, `id_kategori`) VALUES
(20, 'Sunguard', 150000, 'Lenkote Sunguard Acrylic Emulsion adalah cat bermutu tinggi yang dibuat dari 100% bahan superior acrylic yang diformulasikan untuk semua tembok luar baru atau sudah pernah dicat sebelumnya', 1, 1),
(21, 'Everglo', 120000, 'Lenkote Everglo Semi Gloss Luxury Acrylic Emulsion adalah cat tembok berkualitas premium yang memberikan hasil permukaan yang sangat halus, agak mengkilap bagaikan kilauan mutiara', 2, 1),
(22, 'Supersilk', 150000, 'Lenkote Supersilk Super Luxury Acrylic Emulsion adalah cat bermutu tinggi dengan hasil permukaan film akhir halus, tidak mengkilap, warna lebih indah dan tahan lama', 1, 1),
(23, 'No Odor', 120000, 'Lenkote No Odor adalah cat tembok dengan bahan acrylic yang bermutu tinggi sehingga menghasilkan permukaan akhir halus', 1, 1),
(24, 'Aquamatt', 180000, 'Lenkote Aquamatt dibuat dengan konsep “value for money” Pemilihan bahan, penggunaan pigmen serta penciptaan warna dilakukan untuk memaksimalkan keuntungan bagi penggunanya', 1, 1),
(25, 'No Odor Medicare', 180000, 'Lenkote No Odor Medicare adalah cat tembok premium dengan formulasi anti bakteri dan anti kuman sehingga menciptakan ruangan yang aman dari pertumbuhan bakteri dan higienis bagi keluarga Anda', 2, 1),
(26, 'Supersilk Anti Noda', 130000, 'Lenkote Supersilk Anti Noda adalah cat tembok premium yang memiliki efek daun talas, sehingga noda mudah dibersihkan dengan sekali bilas tanpa merusak permukaan cat.', 1, 1),
(27, 'Platinum', 100000, 'Platinum Synthetic adalah cat super kilap yang tidak menguning, dibuat dengan menggunakan bahan resin acrylic 100% yang dapat larut dengan thinner enamel  turpentine maupun thinner A? Seluruh warna-warna pilihan yang terdapat di Platinum Synthetic menggunakan pigmen khusus sehingga memiliki daya tutup yang sangat bagus dan mempunyai ketahanan maksimum terhadap cuaca? Cocok digunakan untuk permukaan kayu dan besi untuk interior maupun eksterior.\nKEGUNAAN DAN KEISTIMEWAAN', 1, 2),
(28, 'Avian Cat Cepat Kering', 210000, 'Avian High Gloss Enamel adalah cat kayu & besi yang sangat mengkilap, cepat kering, daya tutup maksimal, berkualitas tinggi dan terbuat dari bahan alkyd, Cat kayu & besi Avian mempunyai sifat melindungi terhadap cuaca, karat, jamur dan rayap. Sangat cocok digunakan pada pagar, pintu, jendela, kusen, rolling door, dan lain-lain, Koleksi warna-warna cat kayu & besi Avian membawa semangat baru pada hunian anda. Tidak terbatas dengan warna yang pernah ada, tapi berani menciptakan varian warna terbaik untuk anda.', 3, 2),
(29, 'Yoko', 150000, 'Yoko High Gloss Enamel adalah cat kayu & besi berkualitas dengan harga ekonomis. Hadir dengan Blue Improved Formula, keindahan cat kayu & besi Yoko mampu menutup dengan lebih sempurna. Cat kayu & besi Yoko dapat digunakan untuk permukaan dinding eksterior maupun interior dan sangat cocok digunakan untuk pengecatan pada kapal atau furniture.', 3, 2),
(30, 'Glovin', 145000, 'Cat kayu & besi yang terbuat dari bahan alkyd dan pigmen pilihan yang ramah lingkungan dan juga terhadap kesehatan manusia. Menghasilkan hasil akhir pengecatan yang sangat mengkilap dan juga dengan daya tutup maksimal serta tahan cuaca.', 1, 2),
(31, 'No Drop', 145000, 'No Drop adalah cat pelapis anti bocor yang memiliki sifat elastis, kedap air serta sangat tahan cuaca sehingga sangat cocok untuk digunakan pada permukaan tembok, beton, asbes, galvanis/seng dan permukaan terakota.', 4, 3),
(32, 'No Drop Tinting: Cat Pelapis Anti Bocor Berwarna Cerah', 140000, 'No Drop Tinting adalah cat pelapis anti bocor dekoratif yang dapat melindungi tembok dari retak rambut dan rembesan air sekaligus memperindah tembok rumah anda dengan cat warna cerah dan beraneka ragam. Terbuat dari resin yang mengandung acrylic sehingga cocok untuk tembok luar maupun dalam.', 3, 3),
(33, 'No Drop 100', 135000, 'No Drop 100 adalah bahan pelapis anti bocor untuk kamar mandi yang terdiri dari 2 komponen yaitu powder berbasis semen dan liquid berbasis emulsi acrylic.', 2, 3),
(34, 'No Drop Bitumen', 120000, 'No Drop Bitumen Black adalah pelapis bitumen non fiber berbasis air, bersifat elastis dan bermutu tinggi. Memberikan perlindungan pada permukaan beton, plasteran, logam atau besi maupun kayu sebagai pelapis anti bocor, mencegah rembesan air serta anti karat. Setelah mengering No Drop Bitumen Black akan menjadi lapisan bitumen berwarna hitam yang akan melindungi permukaan.', 3, 3),
(35, 'No Drop 107', 150000, 'No Drop 107 adalah bahan pelapis kedap air yang terdiri dari 2 komponen yaitu liquid berbasis emulsi acrylic dan powder berbasis semen, aditif dan filler. No Drop 107 sangat cocok untuk digunakan pada permukaan yang selalu atau sangat sering terendam air, seperti basement, lantai dasar kamar mandi, bak mandi, kolam renang, dak beton atau bagian-bagian bangunan yang rawan kebocoran, beton luar dan dalam tendon air, kolam ikan sebelum diplester atau dikeramik.', 1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `recent_product`
--

CREATE TABLE `recent_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `recent_product`
--

INSERT INTO `recent_product` (`id`, `product_id`) VALUES
(13, 20),
(14, 20),
(15, 20),
(16, 20),
(17, 20),
(18, 20),
(19, 20),
(20, 20),
(22, 20),
(23, 20),
(24, 20),
(25, 20),
(26, 20),
(21, 21),
(27, 21),
(28, 21),
(29, 21),
(2, 22),
(11, 22),
(12, 22),
(30, 22),
(31, 22),
(32, 23),
(33, 23),
(34, 23),
(35, 23),
(36, 24),
(37, 24),
(38, 24),
(1, 25),
(4, 25),
(39, 25),
(5, 26),
(6, 26),
(7, 26),
(8, 26),
(3, 27),
(9, 27),
(10, 27),
(40, 27),
(41, 27),
(42, 28),
(43, 28),
(45, 31),
(44, 34),
(46, 35),
(47, 35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok`
--

CREATE TABLE `stok` (
  `idstok` int(4) NOT NULL,
  `id_produk` int(4) NOT NULL,
  `stoktersedia` int(4) NOT NULL,
  `stokmasuk` int(4) NOT NULL,
  `stokkeluar` int(4) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok`
--

INSERT INTO `stok` (`idstok`, `id_produk`, `stoktersedia`, `stokmasuk`, `stokkeluar`, `tanggal`) VALUES
(1, 22, 5, 0, 0, '2019-12-19'),
(2, 21, 5, 0, 0, '2019-12-19'),
(3, 20, 5, 0, 0, '2019-12-19'),
(57, 20, 4, 0, 1, '2019-12-19'),
(58, 23, 5, 5, 0, '2019-12-20'),
(59, 24, 7, 7, 0, '2019-12-27'),
(60, 25, 10, 10, 0, '2019-12-27'),
(61, 25, 9, 0, 1, '2020-03-10'),
(62, 20, 3, 0, 1, '2020-03-10'),
(63, 20, 2, 0, 1, '2020-03-10'),
(64, 20, 6, 4, 0, '2020-03-10'),
(65, 22, 4, 0, 1, '2020-03-10'),
(66, 26, 4, 4, 0, '2020-03-18'),
(67, 20, 15, 9, 0, '2020-03-20'),
(68, 27, 50, 50, 0, '2020-03-25'),
(69, 28, 50, 50, 0, '2020-03-26'),
(70, 29, 43, 43, 0, '2020-03-26'),
(71, 30, 26, 26, 0, '2020-03-26'),
(72, 31, 45, 45, 0, '2020-03-26'),
(73, 32, 29, 29, 0, '2020-03-26'),
(74, 33, 16, 16, 0, '2020-03-26'),
(75, 34, 14, 14, 0, '2020-03-26'),
(76, 35, 19, 19, 0, '2020-03-26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nohp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `nohp`, `alamat`, `status`) VALUES
(10, 'Tia', 'ara@mail.com', '$2y$10$UNCvENMHekiVq5oEplvHdeCo6Claoj/8Xr3fE7yyK17kj8bSAQ7LS', 'V1H7BcDBLVxciQGgcBWl23mNKKiK1rkOKo2yF05xESjqV74snZnKGxx4J0OB', '2019-02-10 01:42:31', '2019-02-10 01:42:31', '10928176241', 'ajsdaojd', 'user'),
(11, 'Vina', 'garut@mail.com', '$2y$10$gWQp6MsehBhwWCIoJ0eBNOxts3SImFGcJQdVceXRKu9FTrMN5KYS.', '9gJICJqg4mvMzvSrntW4RTENVTSkzFLGpwKbDddAWK2nRzTFm5VNDp6ekrZ5', '2019-08-18 00:16:49', '2019-08-18 00:16:49', '08888888', 'Garut gg 3 RT 1', 'admin'),
(12, 'rijal', 'rijal@mail.com', '$2y$10$aokqTKZtReRQ1IEm7WfNk.c0AtnoWnnUeEvnF9JPUJI.UK0/.KOCi', '8NY5u4jdY2PMQbSmImrAj8m60cuY8VuzZH6lyPd6t7MxQAHkyMssr48obEGY', '2019-08-31 05:21:36', '2019-08-31 05:21:36', '08223132134', 'jalan. mekar sari', 'user'),
(14, 'coba', 'coba@mail.com', '$2y$10$6nmV9UVXsp5CEGbhvRAM3utSrF3gZa5iHT4rpVoE4ouQF5u8N5JcG', NULL, '2019-12-17 23:04:42', '2019-12-17 23:04:42', '08535654', 'jlalanssalkdlkwasd', 'user'),
(15, 'celvin', 'celvinangkie@gmail.com', '$2y$10$CqxDVJ5/8Ct90nWEc02pI.IKZRKpUz6WYiwY8h6YstEpw0iMDphrq', 'NDWKE6MQaRccZomEYSyq2ykTNua1y5mdBHvMdfyMUi8yvT2OTkJ6i3Lah4Dh', '2020-03-10 12:06:50', '2020-03-10 12:06:50', '081241701388', 'rungkut mejoyo utara Ai 18', 'user'),
(16, 'indra', 'indra@gmail.com', '$2y$10$qCV2.3V8JhO81bgIm3cibeUC1Dmvj.yMavbrUuCtt04z2S6tT76tW', 'kmByI8O66R6F2iMwb9NMjl1ApM0Hndw5EhWMvAV53n2Hcdtpa4MktEM1jgZV', '2020-03-24 07:38:58', '2020-03-24 07:38:58', '664', 'fgdfgd', 'user');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warna`
--

CREATE TABLE `warna` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `warna` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `warna`
--

INSERT INTO `warna` (`id`, `nama`, `warna`) VALUES
(1, 'merah', '#ff0000'),
(2, 'kuning', '#ffff00'),
(3, 'putih', '#ffffff'),
(4, 'biru', '#0000ff');

-- --------------------------------------------------------

--
-- Struktur dari tabel `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notabeli`
--
ALTER TABLE `notabeli`
  ADD PRIMARY KEY (`idnota`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `nota_has_produk`
--
ALTER TABLE `nota_has_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`),
  ADD KEY `id_nota` (`id_nota`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`idproduk`),
  ADD KEY `id_warna` (`id_warna`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `recent_product`
--
ALTER TABLE `recent_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`idstok`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `warna`
--
ALTER TABLE `warna`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`),
  ADD KEY `warna` (`warna`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_barang` (`id_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notabeli`
--
ALTER TABLE `notabeli`
  MODIFY `idnota` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `nota_has_produk`
--
ALTER TABLE `nota_has_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `idproduk` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `recent_product`
--
ALTER TABLE `recent_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `idstok` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `warna`
--
ALTER TABLE `warna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_order`
--
ALTER TABLE `detail_order`
  ADD CONSTRAINT `detail_order_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `detail_order_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`idproduk`);

--
-- Ketidakleluasaan untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD CONSTRAINT `gambar_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `notabeli`
--
ALTER TABLE `notabeli`
  ADD CONSTRAINT `notabeli_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `nota_has_produk`
--
ALTER TABLE `nota_has_produk`
  ADD CONSTRAINT `nota_has_produk_ibfk_2` FOREIGN KEY (`id_nota`) REFERENCES `notabeli` (`idnota`),
  ADD CONSTRAINT `nota_has_produk_ibfk_3` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`idproduk`);

--
-- Ketidakleluasaan untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_warna`) REFERENCES `warna` (`id`),
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`);

--
-- Ketidakleluasaan untuk tabel `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `produk` (`idproduk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
