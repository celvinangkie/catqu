<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/beli', 'BeliController@beli')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admtambahproduk', 'ProdukController@admtambahproduk')->name('admtambahproduk');
Route::get('/admmasterbarang', 'ProdukController@admmasterbarang')->name('admmasterbarang');
Route::get('/detailproduk/{idproduk}', 'ProdukController@detproduk')->name('detailproduk');
Route::post('tambah/produk','ProdukController@addProduk');
Route::get('/reqorder', 'ProdukController@reqorder')->name('reqorder');
Route::get('/getmasterbarang', 'ProdukController@getmasterbarang')->name('getmasterbarang');
Route::get('/hapus/produk/{idProduk}','ProdukController@hapusProduk');
Route::post('tambah/stok','ProdukController@tambahStok');
Route::post('tambah/kategori','ProdukController@tambahKategori');
Route::get('get/produk/{idProduk}','ProdukController@getdataproduk');
Route::get('/warna','ProdukController@masterWarna');
Route::post('tambah/warna','ProdukController@tambahwarna');
Route::get('get/produk/all/{warna}','ProdukController@getAllProduk');
Route::get('get/produk/search/{search}','ProdukController@getProdukSearch');
Route::post('request/produk','ProdukController@requestProduk');
Route::get('get/request/produk','ProdukController@getrequestProduk');
Route::get('get/request/produk/user/{iduser}','ProdukController@getuserrequestProduk');
Route::get('cek/request/notif','ProdukController@cekrequestnotif');
Route::get('/historirequestorder','ProdukController@historirequestorder');
Route::get('get/historirequestorder/{iduser}','ProdukController@gethistoriRequestOrderUser');
Route::post('tambah/keranjang','ProdukController@tambahKeranjang');
Route::get('get/keranjang/{iduser}','ProdukController@getKeranjang');
Route::get('count/keranjang/{iduser}','ProdukController@countKeranjang');
Route::get('hapus/keranjang/{iduser}','ProdukController@hapusKeranjang');
Route::get('/prosesproduk','ProdukController@prosesproduk');
Route::post('/beliproduk','ProdukController@beliproduk');
Route::get('/listnotablmlunas/{iduser}','ProdukController@listnotablmlunas');
Route::get('/konfirmasibayar/{idnota}','ProdukController@konfirmasibayar');
Route::post('/bayar','ProdukController@bayar');
Route::get('/setStatusNota/{idnota}/{status}','ProdukController@setStatusNota');
Route::get('/tabelNota/{status}/{halaman}','ProdukController@tabelNota');
Route::get('/detailNota/{idnota}','ProdukController@detailNota');
Route::get('/prosesorder/{idorder}','ProdukController@prosesorder');
Route::get('/batalorder/{idorder}','ProdukController@batalorder');
Route::get('/cobafilter','ProdukController@getAllProdukKategori');
Route::get('/newProduct','HomeController@newProduct');
Route::get('/RecentView','HomeController@RecentView');
Route::get('/kategori/{id}','HomeController@KategoriProduct');
