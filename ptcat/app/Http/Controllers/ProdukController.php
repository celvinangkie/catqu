<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admtambahproduk()
    {
        $warna = DB::table('warna')->get();
        $kategori= DB::table('kategori')->get();
        return view('admtambahproduk',['warna'=>$warna,'kategori'=>$kategori]);     
    }
    // public function getAllProduk($idwarna){
    //     $produk = DB::table('produk')->where('id_warna',$idwarna)->join('gambar','gambar.id_produk','produk.idproduk')->get();
    //     foreach($produk as $produk){
    //         echo '<div class="col-md-4 col-xs-6" >
    //         <div class="shop" style="height:200px">
    //             <div class="shop-img" style="height:200px">
    //                 <img src="'.asset('storage/'.$produk->gambar).'" alt="">
    //             </div>
    //             <div class="shop-body" style="height:200px">
    //             <h3>'.$produk->namabarang.'</h3>
    //             <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
    //             </div>
    //         </div>
    //     </div>';
    //     }
    // }
    public function getAllProdukKategori(Request $request){
        $warna_id = $request->warna_id;
        $kategori = $request->kategori;                
        if($warna_id!="undefined" && $kategori!="undefined"){
   
             //echo "both are selected";
             $produk = DB::table('produk')             
             ->join('gambar','produk.idproduk','=','gambar.id_produk')
             ->where('id_warna',$warna_id)
             ->where('id_kategori',$kategori)
             ->get();
             foreach($produk as $produk){
                echo '<div class="col-md-4 col-xs-6" >
                <div class="shop" style="height:200px">
                    <div class="shop-img" style="height:200px">
                        <img src="'.asset('storage/'.$produk->gambar).'" alt="">
                    </div>
                    <div class="shop-body" style="height:200px">
                    <h3>'.$produk->namabarang.'</h3>
                    <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>';
                }
          }else if($kategori!="undefined"){
   
            //echo "price is selected";
            $produk = DB::table('produk')             
             ->join('gambar','produk.idproduk','=','gambar.id_produk')            
             ->where('id_kategori',$kategori)
             ->get();

             foreach($produk as $produk){
            echo '<div class="col-md-4 col-xs-6" >
            <div class="shop" style="height:200px">
                <div class="shop-img" style="height:200px">
                    <img src="'.asset('storage/'.$produk->gambar).'" alt="">
                </div>
                <div class="shop-body" style="height:200px">
                <h3>'.$produk->namabarang.'</h3>
                <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div>
            </div>';
            }
             
          }
          else if($warna_id!="undefined"){
            //echo "cat is selected";
            $produk = DB::table('produk')             
             ->join('gambar','produk.idproduk','=','gambar.id_produk')
             ->where('id_warna',$warna_id)             
             ->get();
             foreach($produk as $produk){
                echo '<div class="col-md-4 col-xs-6" >
                <div class="shop" style="height:200px">
                    <div class="shop-img" style="height:200px">
                        <img src="'.asset('storage/'.$produk->gambar).'" alt="">
                    </div>
                    <div class="shop-body" style="height:200px">
                    <h3>'.$produk->namabarang.'</h3>
                    <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>';
                }
          }
          else{
            //echo "nothing is slected";
            return "<h1 align='center'>Please select atleast one filter from dropdown</h1>";
   
          }
   
        //   if(count($produk)=="0"){
        //     echo "<h1 align='center'>Tidak ada produk yang dicari</h1>";
        //   }
        
        // $produk = DB::table('produk')->where('id_kategori',$idkategori)->join('gambar','gambar.id_produk','produk.idproduk')->get();
        // foreach($produk as $produk){
        //     echo '<div class="col-md-4 col-xs-6" >
        //     <div class="shop" style="height:200px">
        //         <div class="shop-img" style="height:200px">
        //             <img src="'.asset('storage/'.$produk->gambar).'" alt="">
        //         </div>
        //         <div class="shop-body" style="height:200px">
        //         <h3>'.$produk->namabarang.'</h3>
        //         <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
        //         </div>
        //     </div>
        // </div>';
        // }
    }
    public function getProdukSearch($search){
        if($search == 'all'){
            $produk = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->get();
            foreach($produk as $produk){
                echo '<div class="col-md-4 col-xs-6" >
                <div class="shop" style="height:200px">
                    <div class="shop-img" style="height:200px">
                        <img src="'.asset('storage/'.$produk->gambar).'" alt="">
                    </div>
                    <div class="shop-body" style="height:200px">
                    <h3>'.$produk->namabarang.'</h3>
                    <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>';
            }
        }else{
            $produk = DB::table('produk')->where('produk.namabarang','like',$search.'%')->join('gambar','gambar.id_produk','produk.idproduk')->get();
            foreach($produk as $produk){
                echo '<div class="col-md-4 col-xs-6" >
                <div class="shop" style="height:200px">
                    <div class="shop-img" style="height:200px">
                        <img src="'.asset('storage/'.$produk->gambar).'" alt="">
                    </div>
                    <div class="shop-body" style="height:200px">
                    <h3>'.$produk->namabarang.'</h3>
                    <a href="/detailproduk/'.$produk->idproduk.'" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>';
            }
        }
        
    }
    public function konfirmasibayar($idnota){
        $nota_has_produk = DB::table('nota_has_produk')->where('id_nota',$idnota)->join('produk','produk.idproduk','=','nota_has_produk.id_produk')->get();
        $nota = DB::table('notabeli')->where('idnota',$idnota)->first();
        $new_date = date_format(date_create($nota->due_date), 'd-m-Y');
        return view('konfirmasi',['nota_has_produk'=>$nota_has_produk,'nota'=>$nota,'duedate'=>$new_date]);
    }
    public function addProduk(Request $request)
    {
        // echo $request['nama_produk'].$request['color'];
        $file = $request->gambar->store('storage');

        $Directori = explode("/" , $file);
        $Directori = $Directori[1];
        $id_warna = explode('-',$request['warna']);
        $id_kategori = explode('-',$request['kategori']);
        $idProduk = DB::table('produk')->insertGetId([
            'namabarang' => $request['nama_produk'],
            'harga' => $request['price'],
            'id_warna'=>$id_warna[1],
            'deskripsi'=>$request['deskripsi'],
            'id_kategori'=>$id_kategori[1],
            ]);
        DB::table('stok')->insert([
            'id_produk'=>$idProduk,
            'stokmasuk'=>$request['stok_masuk'],
            'stokkeluar'=>0,
            'stoktersedia'=>$request['stok_masuk'],
            'tanggal'=>date("y-m-d")]);
        DB::table('gambar')->insert(['gambar'=>$Directori,'id_produk'=>$idProduk]);


        return redirect('/admmasterbarang');

    }
    public function hapusProduk($idProduk){
        DB::table('produk')->where('idproduk',$idProduk)->delete();
        return redirect('/admmasterbarang');
    }
    public function admmasterbarang(){
        return view('admmasterbarang');
    }
    public function masterWarna(){
        $warna = DB::table('warna')->get();
        return view('warna',['warna'=>$warna]);

    }
    public function tambahwarna(Request $request){
        DB::table('warna')->insert(['warna'=>$request['warna'],'nama'=>$request['nama']]);
        return redirect('/warna');
    }
    public function detproduk($idproduk){
        $produk = DB::table('produk')->where('idproduk',$idproduk)->first();
        $deskripsi = explode('^^',$produk->deskripsi);
        $gambar = DB::table('gambar')->where('id_produk',$idproduk)->get();
        $stokTersedia = DB::table('stok')->where('id_produk',$idproduk)->latest('idstok')->first();
        $warna = DB::table('warna')->where('id',$produk->id_warna)->first();
        $stokmasuk = $stokTersedia->stokmasuk;
        $stokkeluar = $stokTersedia->stokkeluar;
        $stok = $stokTersedia->stoktersedia;

        $produkrec = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->orderByRaw('RAND()')->limit(3)->get();

        DB::table('recent_product')->insert(['product_id'=>$idproduk]);
        return view('detailproduk',['produk'=>$produk,'deskripsi'=> $deskripsi,'stok'=>$stok,'gambar'=>$gambar,'warna'=>$warna,'produkrec'=>$produkrec]);
    }
    public function reqorder(){
        $detailorder = DB::table('detail_order')->where('status_notif','=','belum')->update(['status_notif'=>'read']);        
        return view('admreqorder');

    }
    public function tambahStok(Request $request){
        
        $stokTersediaSebelumnya = DB::table('stok')        
        ->where('id_produk',$request['idproduk'])->latest('idstok')->first();
        
        $stokTersediaSebelumnya = $stokTersediaSebelumnya->stoktersedia;        
        $stokmasuk = $request['stok'];
        $stoktersediaSaatIni = (int)$stokTersediaSebelumnya + (int)$stokmasuk;
        echo json_encode($stokTersediaSebelumnya);
        echo json_encode($stokmasuk);
        echo json_encode($stoktersediaSaatIni);
            DB::table('stok')->insert([
            'id_produk'=>$request['idproduk'],
            'stokmasuk'=>$stokmasuk,
            'stokkeluar'=>0,
            'stoktersedia'=>$stoktersediaSaatIni,
            'tanggal'=>date("y-m-d")]);
        return redirect('/admmasterbarang');
    }
    public function tambahKategori(Request $request){
        DB::table('kategori')->insert([
            'nama'=>$request['kategori']
            ]);
            return redirect()->back();    
    }
    public function listnotablmlunas($iduser){
        $lisnota = DB::table('notabeli')->where('status','belum lunas')->where('id_user',$iduser)->get();
        echo '<div class="cart-list">';
        foreach ($lisnota as $list) {
            echo '<div class="product-body">
                <h3 class="product-name"><a href="'.url('konfirmasibayar/'.$list->idnota).'">nota-'.$list->idnota.'</a></h3>
                <h4 class="product-price"><span class="qty"></span>Rp.'.$list->grandtotal.'</h4>
            </div><hr>';
        }
        echo '</div>';

    }
    public function listnotalunas(Request $request){

    }
    public function getmasterbarang(){
        $produk = DB::table('produk')->get();
        echo '<table class="table table-striped" class="table" style="width:100%" >
      <tr>
        <th>ID Barang</th>
        <th>Nama Barang</th> 
        <th>Stok Tersedia</th>
        <th>Aksi</th>
      </tr>';
        foreach ($produk as $dataP ) {
        $Datastok = DB::table('stok')        
        ->where('id_produk',$dataP->idproduk)->latest('idstok')->first();        
        $stoktersedia= $Datastok->stoktersedia;
      echo 
      '<tr>
        <td><a href="">'.$dataP->idproduk.'</a></td>
        <td>'.$dataP->namabarang.'</td> 
        <td>'.$stoktersedia.'</td>
        <td>
            <a onclick="editbarang('.$dataP->idproduk.')" href="#" class="btn btn-primary" >Edit</a>
            <a onclick="tambahstok('.$dataP->idproduk.')" href="#" class="btn btn-primary" >Tambah Stok</a>
            <a href="'.url('/hapus/produk/'.$dataP->idproduk).'" class="btn btn-primary" >Delete</a>
        </td>
      </tr>';
      
        }
        echo '</table>';
    }

    public function getdataproduk($idproduk){
        $produk = DB::table('produk')->where('idproduk',$idproduk)->first();
        return json_encode($produk);
    }
    public function requestProduk(Request $request){
        try
    	{
        $produk = DB::table('produk')->where('idproduk',$request['idproduk'])->first();
        $harga = $produk->harga;
        $subtotal = (int)$request['jumlah'] * (int)$harga;
        $idorder = uniqid();
        DB::table('detail_order')
        ->insert(['id_user'=>$request['iduser'],'id_produk'=>$request['idproduk'],
        'id_order'=>$idorder,'tanggal'=>date("y-m-d"),'qty'=>$request['jumlah'],'subtotal'=>$subtotal,'status_order'=>'Request sedang diproses']);
        echo number_format($subtotal,2,',','.');  
        }
        catch(\Exception $e){
        // do task when error
        echo $e->getMessage();   // insert query
        }
    }
    public function getrequestProduk(){
        $detail_order = DB::table('detail_order')->select('id_user')->groupBy('id_user')->get();
        echo '<table class="table table-striped" style="width:100%" >
        <tr>
          <th>ID Customer</th>
          <th>Nama Customer</th> 
          <th>Aksi</th>
        </tr>';
        foreach($detail_order as $detail_order){
            $user = DB::table('users')->where('id',$detail_order->id_user)->first();
            echo '<tr>
            <td>'.$detail_order->id_user.'</td>
            <td>'.$user->name.'</td> 
            <td>
                <a href="#" onclick="viewreqOrder('.$detail_order->id_user.')" class="btn btn-primary" >Lihat Request Order</a>
            </td>
          </tr>';
        }
        echo '</table>';
    }
    public function gethistoriRequestOrderUser($iduser){
        $detail_order = DB::table('detail_order')->where('id_user',$iduser)->get();
        echo '<table class="table table-striped" style="width:100%" >
        <tr>
          <th>No Order</th>
          <th>Nama Produk</th>
          <th>Nama Warna</th> 
          <th>Warna</th>
          <th>tanggal order</th>
          <th>qty</th>
          <th>subtotal</th>
          <th>status_order</th>
        </tr>';
        foreach($detail_order as $detail_order){

            $produk = DB::table('produk')->join('warna','warna.id','produk.id_warna')->
            where('idproduk',$detail_order->id_produk)->first();
            echo '<tr>
            <td>'.$detail_order->id_order.'</td>
            <td>'.$produk->namabarang.'</td>
            <td>'.$produk->nama.'</td>
            <td><div class="col-md-2" >
            <div class="col-md-1" id="divwarna" style="border-style: solid; background-color:'.$produk->warna.';" >
               <p></p>
            </div>
         </div></td>
            <td>'.$detail_order->tanggal.'</td>
            <td>'.$detail_order->qty.'</td>
            <td>'.$detail_order->subtotal.'</td> 
            <td>'.$detail_order->status_order.'</td> 
          </tr>';
        }
        echo '</table>';
    }
    public function getuserrequestProduk($iduser){
        $detail_order = DB::table('detail_order')->where('id_user',$iduser)->get();
        echo '<table class="table table-striped" style="width:100%" >
        <tr>
          <th>No Order</th>
          <th>Nama Produk</th>
          <th>Nama Warna</th> 
          <th>Warna</th>
          <th>tanggal order</th>
          <th>qty</th>
          <th>subtotal</th>
          <th>status_order</th>
          <th>stok produk</th>
          <th>Aksi</th>
        </tr>';
        foreach($detail_order as $detail_order){

            $Datastok = DB::table('stok')->where('id_produk',$detail_order->id_produk)
                     ->select('id_produk',DB::raw('SUM(stokmasuk) as stokmasuk'),
                        DB::raw('SUM(stokkeluar) as stokkeluar'))
                     ->groupBy('id_produk')->first();
            $stokmasuk = $Datastok->stokmasuk; 
            $stokkeluar = $Datastok->stokkeluar;
            $stok = (int)$stokmasuk - (int)$stokkeluar;

            $produk = DB::table('produk')->join('warna','warna.id','produk.id_warna')->
            where('idproduk',$detail_order->id_produk)->first();
            echo '<tr>
            <td>'.$detail_order->id_order.'</td>
            <td>'.$produk->namabarang.'</td>
            <td>'.$produk->nama.'</td>
            <td><div class="col-md-2" >
            <div class="col-md-1" id="divwarna" style="border-style: solid; background-color:'.$produk->warna.';" >
               <p></p>
            </div>
         </div></td>
            <td>'.$detail_order->tanggal.'</td>
            <td>'.$detail_order->qty.'</td>
            <td>'.$detail_order->subtotal.'</td> 
            <td>'.$detail_order->status_order.'</td> 
            <td>'.$stok.'</td>';
            if($detail_order->status_order == 'Request sedang diproses'){
                echo '<td>
                    <a href="'.url('/prosesorder/'.$detail_order->id).'" class="btn btn-primary" >Proses Produk</a>
                    <br>
                    <a href="'.url('/batalorder/'.$detail_order->id).'" class="btn btn-primary" >Batal Produk</a>
                </td>';  
            }
        echo '</tr>';
        }
        echo '</table>';
    }
    public function tambahKeranjang(Request $request){
        try
    	{
        $produk = DB::table('produk')->where('idproduk',$request['idproduk'])->first();
        $harga = $produk->harga;
        $subtotal = (int)$request['jumlah'] * (int)$harga;
        $idorder = uniqid();
        DB::table('keranjang')
        ->insert(['id_user'=>$request['iduser'],'id_produk'=>$request['idproduk']
        ,'qty'=>$request['jumlah'],'subtotal'=>$subtotal]);
        echo 'Berhasil tambah ke keranjang';
        }
        catch(\Exception $e){
        // do task when error
        echo $e->getMessage();   // insert query
        }
    }
    
    public function getKeranjang($iduser){
        $keranjang = DB::table('keranjang')->where('id_user',$iduser)
        ->join('produk','keranjang.id_produk','=','produk.idproduk')->get();
        echo '<div class="cart-list">
        ';
        $grandtotal = 0;
        foreach($keranjang as $data){
            $gambar = DB::table('gambar')->where('id_produk',$data->id_produk)->first();
            echo '<div class="product-widget"><div class="product-img">
            <img src="'.asset('storage/'.$gambar->gambar).'" alt="">
            </div>';
            $subtotal = $data->subtotal;
            $grandtotal += (int)$subtotal;
            echo '<div class="product-body">
            <h3 class="product-name"><a href="#">'.$data->namabarang.'</a></h3>
            <h4 class="product-price"><span class="qty">'.$data->qty.'x </span>Rp.'.$data->harga.'</h4>
            <h4 class="product-price">Subtotal: Rp.'.$data->subtotal.'</h4>
            </div>
            <button onclick="hapuskeranjang('.$data->id.')" class="delete"><i class="fa fa-close"></i></button>
            </div>';
        }
        echo '</div>
        <div class="cart-summary">
            <h5>Grand Total : '.$grandtotal.'</h5>
        </div>
        <div class="cart-btns">
            <a href="'.url('prosesproduk').'">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
        </div>';
    }

    public function hapusKeranjang($idkeranjang){
        DB::table('keranjang')->where('id',$idkeranjang)->delete();
    }

    public function prosesproduk(){
        $user = Auth::user();
        $keranjang = DB::table('keranjang')->where('id_user',$user->id)->join('produk','keranjang.id_produk','=','produk.idproduk')->get();
        $arrayProduk = [];
        $i = 0;
        $grandtotal = 0;
        foreach($keranjang as $dataK){
            $stok = DB::table('stok')->where('id_produk',$dataK->id_produk)->orderBy('idstok', 'DESC')->first();
            $stokBarang = $stok->stoktersedia;
            $jumlahBarang = $dataK->qty;
            $id_produk = $dataK->id_produk;
            $subtotal = $dataK->subtotal;
            if((int)$jumlahBarang<(int)$stokBarang){
                $grandtotal += (int)$subtotal;
                $arrayProduk[$i] = [$id_produk,$jumlahBarang,$subtotal,$dataK->id];
                $i++;
            }else{
                echo 'stok '.$keranjang->namabarang.' kurang; ';
            }
            
        }
        if(!empty($arrayProduk) ){
            $datenow = date("Y-m-d");
            $duedate = date( "Y-m-d", strtotime($datenow."+2 day" ));
            $idnota = DB::table('notabeli')->insertGetId(['id_user'=>$user->id,'tanggal'=>$datenow,'grandtotal'=>$grandtotal,'due_date'=>$duedate]);
            for($j=0; $j<count($arrayProduk); $j++){
                DB::table('nota_has_produk')->insert(['id_produk'=>$arrayProduk[$j][0],
                'id_nota'=>$idnota,'subtotal'=>$arrayProduk[$j][2],'qty'=>$arrayProduk[$j][1]]);
                $stok = DB::table('stok')->where('id_produk',$arrayProduk[$j][0])->orderBy('idstok', 'DESC')->first();
                $stokBarang = $stok->stoktersedia;
                $jumlahBarang = $arrayProduk[$j][1];
                $stoksaatini = (int)$stokBarang - (int)$jumlahBarang;
                DB::table('stok')->insert(['id_produk'=>$arrayProduk[$j][0],
                'stokmasuk'=>0,'stokkeluar'=>$arrayProduk[$j][1],
                'tanggal'=>$datenow,'stoktersedia'=>$stoksaatini]);
                DB::table('keranjang')->where('id',$arrayProduk[$j][3])->delete();
            }
        }
        return redirect('/');
        
    }

    public function beliproduk(Request $request){
        $user = Auth::user();
        $stok = DB::table('stok')->where('id_produk',$request['idproduk'])->orderBy('idstok', 'DESC')->first();
        $stokBarang = $stok->stoktersedia;
        $jumlahBarang = $request['jumlah'];

        $produk = DB::table('produk')->where('idproduk',$request['idproduk'])->first();
        $harga = $produk->harga;
        $subtotal = (int)$request['jumlah'] * (int)$harga;

        if((int)$jumlahBarang<(int)$stokBarang){
            $datenow = date("Y-m-d");
            $duedate = date( "Y-m-d", strtotime($datenow."+2 day" ));
            $idnota = DB::table('notabeli')->insertGetId([
            'id_user'=>$user->id,'tanggal'=>$datenow,'grandtotal'=>$subtotal,'due_date'=>$duedate]);
            $stoksaatini = (int)$stokBarang - (int)$jumlahBarang;
            DB::table('nota_has_produk')->insert(['id_produk'=>$request['idproduk'],
                'id_nota'=>$idnota,'subtotal'=>$subtotal,'qty'=>$jumlahBarang]);
            DB::table('stok')->insert(['id_produk'=>$request['idproduk'],
                'stokmasuk'=>0,'stokkeluar'=>$jumlahBarang,
                'tanggal'=>$datenow,'stoktersedia'=>$stoksaatini]);
                
        }
        echo 'berhasil';
        
    }

    public function checkout(){

    }

    public function cekrequestnotif(){
        $jumlahNotif = DB::table('detail_order')->where('status_notif','=','belum')->count();
       
        echo $jumlahNotif;
    }
    public function countKeranjang($iduser){
        $jumlah = DB::table('keranjang')->where('id_user','=',$iduser)->count();
       
        echo $jumlah;
    }
    public function historirequestorder(){
        return view('historyrequestorder');
    }
    public function countBayar(){

    }
    public function bayar(Request $request ){
        $file_name = $request->file('bukti')->getClientOriginalName();
            $file = $request->bukti->store('public');
        $Directori = explode("/" , $file);
        $Directori = $Directori[1];
        DB::table('notabeli')->where('idnota',$request['idnota'])->update(['alamat'=>$request['alamat'],'catatan'=>$request['catatan'],'bukti_bayar'=>$Directori,'status'=>'Sudah Bayar']);
        return redirect('/');
    }
    public function setStatusNota($idnota,$status){
        DB::table('notabeli')->where('idnota',$idnota)->update(['status'=>$status]);
        return redirect()->back();
    }
    public function tabelNota($status,$halaman){
        $user = Auth::user();
        if($status =="semua"){
            if($user->status == 'admin'){
                $notabeli = DB::table('notabeli')->join('users','notabeli.id_user','=','users.id')->select('notabeli.*','users.name')->get();
            }else{
                $notabeli = DB::table('notabeli')->join('users','notabeli.id_user','=','users.id')->where('users.id',$user->id)->select('notabeli.*','users.name')->get();
            }
            
        }else{
            if($user->status == 'admin'){
                $notabeli = DB::table('notabeli')->join('users','notabeli.id_user','=','users.id')->where('notabeli.status',$status)->select('notabeli.*','users.name')->get();
                echo json_encode($status);
            }else{
                $notabeli = DB::table('notabeli')->join('users','notabeli.id_user','=','users.id')->where('users.id',$user->id)->where('notabeli.status',$status)->select('notabeli.*','users.name')->get();
                echo json_encode($status);
            }
               
        }
        if($halaman == "KonfirmasiPembayaran"){
            return view('tabelnota',['notabelis'=>$notabeli,'halaman'=>$halaman]);
        }
        else if($halaman == "HistoryPembayaran"){
            return view('tabelnota',['notabelis'=>$notabeli,'halaman'=>$halaman]);
        }
        else if($halaman == "DataPenjualan"){
            $penjualan = DB::select(DB::raw("SELECT P.namabarang,SUM(NP.qty) AS total
            FROM produk P JOIN nota_has_produk NP ON P.idproduk=NP.id_produk JOIN notabeli NB ON NP.id_nota=NB.idnota 
            WHERE NB.status != 'belum lunas'
            GROUP BY P.namabarang"));
            return view('tabelnota',['penjualan'=>$penjualan,'halaman'=>$halaman]);
        }
        else if($halaman == "TerimaProduk"){
            return view('tabelnota',['notabelis'=>$notabeli,'halaman'=>$halaman]);            
        }else if($halaman =="HistoryPembelian"){
            return view('tabelnota',['notabelis'=>$notabeli,'halaman'=>$halaman]);
        }else if($halaman =="StatusPembayaran"){
            return view('tabelnota',['notabelis'=>$notabeli,'halaman'=>$halaman]);
        }
    }
    public function detailNota($idnota){
        DB::table('nota_has_produk')->where('id_nota',$idnota)->get();
    }
    public function prosesorder($idorder){
        $datenow = date("Y-m-d");
        $duedate = date( "Y-m-d", strtotime($datenow."+2 day" ));
        $order = DB::table('detail_order')->where('id',$idorder)->first();
        $stok = DB::table('stok')->where('id_produk',$order->id_produk)->orderBy('idstok', 'DESC')->first();
        $stokBarang = $stok->stoktersedia;
        $jumlahBarang = $order->qty;
        $stoksaatini = (int)$stokBarang - (int)$jumlahBarang;
        if($stoksaatini <0){
            $detailorder = DB::table('detail_order')->where('status_notif','=','belum')->update(['status_notif'=>'read']);
            return view('admreqorder',['pesan'=>'Stok Kurang']);
        }else{

            $idnota = DB::table('notabeli')->insertGetId(['id_user'=>$order->id_user,'tanggal'=>$datenow,'grandtotal'=>$order->subtotal,'due_date'=>$duedate]);
            DB::table('nota_has_produk')->insert([
                'id_produk'=>$order->id_produk,
                'id_nota'=>$idnota,
                'qty'=>$order->qty,
                'subtotal'=>$order->subtotal]);
            DB::table('detail_order')->where('id',$order->id)
            ->update(['status_order'=>'Sudah diproses']);
            DB::table('stok')->insert(['id_produk'=>$order->id_produk,
            'stokmasuk'=>0,'stokkeluar'=>$jumlahBarang,
            'tanggal'=>$datenow,'stoktersedia'=>$stoksaatini]);
            return redirect()->back();    
        }
        
        
    }
    public function batalorder($idorder){
        $order = DB::table('detail_order')->where('id',$idorder)->first();
        DB::table('detail_order')->where('id',$order->id)
            ->update(['status_order'=>'Order Dibatalkan']);
            return redirect()->back();   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
