<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        
        if($user==null){
            $warna = DB::table('warna')->get();
            $kategori = DB::table('kategori')->get();
            $produk = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->get();
            $enableSearch = true;
            return view('index',['produk'=>$produk,'kategori'=>$kategori,'warna'=>$warna,'enableSearch'=>$enableSearch]);

        }
        
        if ($user->status=='admin') {
            // $warna = DB::table('warna')->get();
            Session::put('iduser', $user->id);
            return view('admindex');
            
        }
        else {  
            $warna = DB::table('warna')->get();  
            $kategori = DB::table('kategori')->get();       
            $produk = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->get();
            Session::put('iduser', $user->id);
            $enableSearch = true;
            return view('index',['produk'=>$produk,'kategori'=>$kategori,'warna'=>$warna,'enableSearch'=>$enableSearch]);
            
            
        }
        
    }
    public function newProduct(){
            $warna = DB::table('warna')->get();  
            $kategori = DB::table('kategori')->get();       
            $produk = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->orderBy('id','desc')->limit(3)->get();
            // Session::put('iduser', $user->id);
            $enableSearch = true;
            return view('newProduct',['produk'=>$produk,'kategori'=>$kategori,'warna'=>$warna,'enableSearch'=>$enableSearch]);
    }
    public function RecentView(){
            $warna = DB::table('warna')->get();  
            $kategori = DB::table('kategori')->get();       
            $produk = DB::table('recent_product')->join('produk','produk.idproduk','recent_product.product_id')->join('gambar','gambar.id_produk','produk.idproduk')->orderBy('recent_product.id','desc')->limit(4)->get();
            // Session::put('iduser', $user->id);
            $enableSearch = true;
            return view('newProduct',['produk'=>$produk,'kategori'=>$kategori,'warna'=>$warna,'enableSearch'=>$enableSearch]);
    }
    public function KategoriProduct($id){
            $warna = DB::table('warna')->get();  
            $kategori = DB::table('kategori')->get();
            $namakategori = DB::table('kategori')->where('id',$id)->first();       
            $produk = DB::table('produk')->join('gambar','gambar.id_produk','produk.idproduk')->orderBy('id','desc')->where('id_kategori',$id)->get();
            // Session::put('iduser', $user->id);
            $enableSearch = true;
            return view('kategoriProduct',['produk'=>$produk,'kategori'=>$kategori,'warna'=>$warna,'enableSearch'=>$enableSearch,'namakategori'=>$namakategori->nama]);
    }
    public function insertRecentView(){

    }
}

