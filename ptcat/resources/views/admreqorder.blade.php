@extends('layouts.app')
@section('judul') 
<li><a href="#">Home</a></li> <!-- ini dan bawah diganti tiap halaman -->
<li>Request Order</li>
@endsection
@section('content')
    <style>
        table, th, td {border: 1px solid black; text-align: center;}
    </style>
    <div class="modal fade" id="vieworder" role="dialog" >
        <div class="modal-dialog" style="width: 1000px;
        margin: auto;">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">List Request Order</h4>
            </div>
            <div class="modal-body">
              <div id="orderuser">

              </div>   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
    <div id="tabelreorder">
    
    </div>
    
    {{-- <table style="width:100%" >
      <tr>
        <th>ID Customer</th>
        <th>Nama Customer</th> 
        <th>Aksi</th>
      </tr>
      <tr>
        <td><a href="">01032103</a></td>
        <td>Cat A</td> 
        <td>
            <a href="#" class="btn btn-primary" >Order Selesai</a>
        </td>
      </tr>
    </table> --}}
    
@endsection
@section('script')
    <script>
       @if(isset($pesan))
          alert("{{$pesan}}");
        @endif
      $(function(){

      $.get("{{url('get/request/produk')}}",function(data){
        $('#tabelreorder').html(data);            
      });
      });
      function viewreqOrder(iduser){
        $.get("{{url('get/request/produk/user/')}}/"+iduser,function(data){
          $('#orderuser').html(data);            
        });
        $('#vieworder').modal('show');
      }
    </script>
@endsection