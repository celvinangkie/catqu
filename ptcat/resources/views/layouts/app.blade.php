<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="{{asset('electro/css/bootstrap.min.css')}} "/>


        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="{{asset('electro/css/slick.css')}}"/>
        <link type="text/css" rel="stylesheet" href="{{asset('electro/css/slick-theme.css')}}"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="{{asset('electro/css/nouislider.min.css')}}"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="{{asset('electro/css/font-awesome.min.css')}}"/>

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="{{asset('electro/css/style.css')}}"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    

    <body>
        <!-- HEADER -->
        <header>
            <!-- TOP HEADER -->
            <div id="top-header">
                <div class="container">
                    <ul class="header-links pull-left">
                        <li><a href="#"><i class="fa fa-phone"></i> +62(0)214605710</a></li>
                        <li><a href="#"><i class="fa fa-envelope-o"></i> ‎catqu@ctq.com </a></li>
                        <li><a href="#"><i class="fa fa-map-marker"></i> Mangrove, Surabaya Timur</a></li>
                    </ul>
                    <ul class="header-links pull-right">
                @if (Route::has('login'))
                            
                        @auth
                            <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                            <li><a href="{{ route('logout') }}"onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li>
                        @else
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>

                        @endauth
                    </div>
                 @endif
                        <!-- <li><a href="#"><i class="fa fa-dollar"></i> USD</a></li> -->
                        
                        
                    </ul>

                </div>
            </div>
            <!-- /TOP HEADER -->

            <!-- MAIN HEADER -->
            <div id="header">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                        @if (Route::has('login'))    
                        @auth
                            @if(Auth::user()->status == 'admin')
                        <!-- LOGO -->
                        <div class="col-md-3">
                            <div class="header-logo">
                                <a href="{{ url('/home') }}" class="logo">
                                    <img src={{asset('gambar/catqu.JPG')}} alt="" width="169px" height="70px">
                                    
                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->
                            @else
                            <div class="row">
                        <!-- LOGO -->
                        <div class="col-md-3">
                            <div class="header-logo">
                                <a href="{{ url('/') }}" class="logo">
                                    <img src={{asset('gambar/catqu.JPG')}} alt="" width="169px" height="70px">
                                    
                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->
                        @endif
                        @else
                        <div class="row">
                        <!-- LOGO -->
                        <div class="col-md-3">
                            <div class="header-logo">
                                <a href="{{ url('/') }}" class="logo">
                                    <img src={{asset('gambar/catqu.JPG')}} alt="" width="169px" height="70px">
                                    
                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->
                            
                        @endauth
                        
                        @endif

                        <!-- SEARCH BAR -->
                       
                        <div class="col-md-6" >
                            @isset($enableSearch)
                            <div class="header-search">
                                <form>
                                    <div class="col-md-12">
                                        <input  class="input-select col-md-9" placeholder="Search here">
                                        <button class="search-btn col-md-3">Search</button>
                                    </div>
                                </form>
                            </div>
                            @endisset
                        </div>
                        <!-- /SEARCH BAR -->

                        <!-- ACCOUNT -->
                        <div class="col-md-3 clearfix">
                            <div class="header-ctn">
                                @if (Route::has('login'))
                            
                                @auth
                                    @if(Auth::user()->status == 'admin')
                                <div>
                                    <a href="{{url('admmasterbarang')}}">
                                        <img src={{asset('gambar/tbh.png')}} alt="" width="40px" height="40px"><br>
                                        <span>Master Barang</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{url('reqorder')}}">
                                        <img src={{asset('gambar/order.png')}} alt="" width="40px" height="40px"><br><br>
                                        <span>Request Order</span>
                                        <div  id="qty" class="qty"></div>
                                    </a>
                                </div>
                                @else

                                <div class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-money"></i>
                                        <span>Pembayaran</span>
                                        <div id="countbayar" class="qty"></div>
                                    </a>
                                    <div class="cart-dropdown" id="listnota">
                                        
                                    </div>
                                </div>
                                <!-- /Wishlist -->

                                <!-- Cart -->
                                <div class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span>Cart</span>
                                        <div id="cart" class="qty"></div>
                                    </a>
                                    <div class="cart-dropdown" id="keranjang">
                                        
                                    </div>
                                </div>
                              
                                
                                <!-- /Cart -->

                                @endif

                                @endauth
                        
                                @endif
                                
                                <!-- Wishlist -->
                                
                                <!-- Menu Toogle -->
                                <div class="menu-toggle">
                                    <a href="#">
                                        <i class="fa fa-bars"></i>
                                        <span>Menu</span>
                                    </a>
                                </div>
                                <!-- /Menu Toogle -->
                            </div>
                        </div>
                        <!-- /ACCOUNT -->
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </div>
            <!-- /MAIN HEADER -->
        </header>
        <!-- /HEADER -->

        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">

                        <li id="home" ><a href="/home">Home</a></li>
                        @if (Route::has('login'))
                            @auth
                            @if(Auth::user()->status == 'admin')
                                <li id=""><a href="{{url('tabelNota')}}\Sudah Bayar\KonfirmasiPembayaran">Konfirmasi Pembayaran</a></li>
                                <li id=""><a href="{{url('tabelNota')}}\semua\HistoryPembayaran">History Pembayaran</a></li>
                                <li id=""><a href="{{url('tabelNota')}}\semua\DataPenjualan">Data Penjualan</a></li>
                            @else
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Kategori Product
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="navbarDropdown">
                                      <a class="dropdown-item" href="{{url('kategori/1')}}"> Cat Tembok Premium</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item" href="{{url('kategori/2')}}">Cat Kayu dan Besi</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item" href="{{url('kategori/3')}}">Cat Pelapis Anti Bocor</a>
                                    </div>
                                  </li>
                                <li ><a href="{{url('newProduct')}}">New Product</a></li>
                                <li ><a href="{{url('RecentView')}}">Recent view</a></li>
                                <li id="histori"><a href="{{url('historirequestorder')}}">Request Order</a></li>
                                <li id=""><a href="{{url('tabelNota')}}\Sudah Bayar\StatusPembayaran">Status Pembayaran</a></li>
                                <li id="histori"><a href="{{url('tabelNota')}}\menunggu pengambilan\TerimaProduk">Terima Produk</a></li>
                                <li id=""><a href="{{url('tabelNota')}}\barang diambil\HistoryPembelian">History Pembelian</a></li>
                                <li id=""><a href="">User Manual</a></li>
                            @endif
                            @endauth
                        @endif
                        <!-- <li><a href="#">Laptops</a></li>
                        <li><a href="#">Smartphones</a></li>
                        <li><a href="#">Cameras</a></li>
                        <li><a href="#">Accessories</a></li> -->
                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->

        <!-- BREADCRUMB -->
        <div id="breadcrumb" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb-tree">
                            @yield('judul')
                            
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /BREADCRUMB -->

        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                @yield('content')
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- NEWSLETTER -->
        <!-- <div id="newsletter" class="section"> -->
            <!-- container -->
            <!-- <div class="container"> -->
                <!-- row -->
             <!--    <div class="row">
                    <div class="col-md-12">
                        <div class="newsletter">
                            <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                            <form>
                                <input class="input" type="email" placeholder="Enter Your Email">
                                <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                            </form>
                            <ul class="newsletter-follow">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div> -->
                <!-- </div> -->
                <!-- /row -->
           <!--  </div> -->
            <!-- /container -->
        <!-- </div> -->
        <!-- /NEWSLETTER -->

        <!-- FOOTER -->
        <footer id="footer">
            <!-- top footer -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-title">About Us</h3>
                                <p>PT. PPG Coatings Indonesia adalah sebuah perusahaan yang bergerak pada bidang penjualan cat kapal. </p>
                                <ul class="footer-links">
                                    <li><a href="#"><i class="fa fa-map-marker"></i>Mangrove, Surabaya Timur</a></li>
                                    <li><a href="#"><i class="fa fa-phone"></i>+62(0)214605710</a></li>
                                    <li><a href="#"><i class="fa fa-envelope-o"></i>catqu@ctq.com</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-title">Categories</h3>
                                <ul class="footer-links">
                                    <li><a href="#">Hot deals</a></li>
                                    <li><a href="#">Cat Terbaru</a></li>
                                    <!-- <li><a href="#">Smartphones</a></li>
                                    <li><a href="#">Cameras</a></li>
                                    <li><a href="#">Accessories</a></li> -->
                                </ul>
                            </div>
                        </div>

                        <div class="clearfix visible-xs"></div>

                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-title">Information</h3>
                                <ul class="footer-links">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Orders and Returns</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-title">Service</h3>
                                <ul class="footer-links">
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">View Cart</a></li>
                                    <li><a href="#">Wishlist</a></li>
                                    <li><a href="#">Track My Order</a></li>
                                    <li><a href="#">Help</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /top footer -->

            <!-- bottom footer -->
            <div id="bottom-footer" class="section">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <ul class="footer-payments">
                                <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                                <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                                <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                                <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                                <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                                <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                            </ul>
                            <span class="copyright">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </span>


                        </div>
                    </div>
                        <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /bottom footer -->
        </footer>
        <!-- /FOOTER -->

        <!-- jQuery Plugins -->
        <script src="{{asset('electro/nouislider.min.js')}}"></script>
        <script src="{{asset('electro/js/jquery.zoom.min.js')}}"></script>
        <script src="{{asset('electro/js/main.js')}}"></script>
        <script src="{{asset('electro/js/jquery.min.js')}}"></script>
		<script src="{{asset('electro/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('electro/js/slick.min.js')}}"></script>
        <script>
            
            $(function() { 
                $.get( "{{url('cek/request/notif')}}", function( data ) {
                    var notif = parseInt(data);
                    if(notif >0){
                        $('#qty').show();
                        $('#qty').html(notif);
                    }else{
                        $('#qty').hide();
                    }
                });
                @auth
                $.get( "{{url('get/keranjang/'.Auth::user()->id)}}", function( data ) {
                    $('#keranjang').html(data);
                });
                $.get( "{{url('/listnotablmlunas/'.Auth::user()->id)}}", function( data ) {
                    $('#listnota').html(data);
                });

                $.get( "{{url('count/keranjang/'.Auth::user()->id)}}", function( data ) {
                    var notif = parseInt(data);
                    if(notif >0){
                        $('#cart').show();
                        $('#cart').html(notif);
                    }else{
                        $('#cart').hide();
                    }
                });
                @endauth

            });
            function hapuskeranjang(idkeranjang){
                $.get( "{{url('hapus/keranjang')}}/"+idkeranjang, function( data ) {
                    location.reload();
                });
            }
            
        </script>
        @yield('script')
    </body>
</html>
