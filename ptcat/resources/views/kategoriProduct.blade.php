@extends('layouts.app')
@section('judul') 
<li><a href="/">{{$namakategori}}</a></li> <!-- ini dan bawah diganti tiap halaman -->
@endsection
@section('search')
<div class="col-md-6" >
		<div class="header-search">
			<form>
				<select class="input-select">
					<option value="0">All Categories</option>
					<option value="1">Category 01</option>
					<option value="1">Category 02</option>
				</select>
				<input class="input" placeholder="Search here">
				<button class="search-btn">Search</button>
			</form>
		</div>
	</div>	
@endsection
@section('content')
	<div class="section">
			<!-- container -->
			<div class="container">
					<div class="row">
					<div class="form-group col-md-6">
							
							<div class="col-md-3">
								  <select  id="warna" name="warna" >
										<option value="">Pilih Warna</option>
									 @foreach ($warna as $warna)
										<option value="{{$warna->warna}}-{{$warna->id}}">{{$warna->nama}}</option>
									 @endforeach
									 
								  </select>
								  
								  <span class="help-block with-errors"></span>
							   </div>
							<div class="col-md-2" >
							   <div class="col-md-1" id="divwarna" style="" >
								  <p></p>
							   </div>
							</div>

<!-- 							<div class="col-md-3" >
								<select  id="kategori" name="kategori" >
									<option value="">Pilih Kategori</option>
								 @foreach ($kategori as $kategori)
									<option value="{{$kategori->id}}">{{$kategori->nama}}</option>
								 @endforeach
								 
							  </select>
							 </div> -->
							 <div class="col-md-3" >
								<button id="findbtn"> Find </button>
							 </div>
					  </div>
					</div>
				</div>
				<!-- row -->
				<div class="row" id="produk">
					<!-- shop -->
					@foreach ($produk as $produk)
					<div class="col-md-4 col-xs-6" >
						<div class="shop" style="height:200px">
							<div class="shop-img" style="height:200px">
								<img src={{asset('storage/'.$produk->gambar)}} alt="">
							</div>
							<div class="shop-body" style="height:200px">
							<h3>{{$produk->namabarang}}</h3>
							<a href="/detailproduk/{{$produk->idproduk}}" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
					@endforeach

					
					<!-- /shop -->

					<!-- shop -->
					
					<!-- /shop -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
@endsection
@section('script')
<script>
		// $(document).ready(function(){
		//   $("#warna").change(function(){
		// 	 if($('#warna').val()==''){
		// 		$('#divwarna').attr('style','');
		// 	 }else{
		// 		var arrwarna = $('#warna').val().split("-");
		// 		var arrkategori = $('#kategori').val().split("-");
		// 		warna = arrwarna[0];
				
		// 		$.get( "{{url('get/produk/all')}}/"+arrwarna[1], function( data ) {
		// 			$( "#produk" ).html( data );
					
		// 		});
				
		// 		$('#divwarna').attr('style','border-style: solid; background-color:'+warna);
		// 	 }
		//   });
		// });
		$(document).ready(function(){

			$("#findbtn").click(function(){
			var arrwarna = $('#warna').val().split("-");			
			warna = arrwarna[0];		
			var kategori = $('#kategori').val();			
				$.ajax({
					type: 'get',
					dataType: 'html',
					url: '{{url('/cobafilter')}}',
					data: 'warna_id=' + arrwarna[1] + '&kategori=' + kategori,
					success:function(data){
					console.log(data);					
					$("#produk").html(data);
					}
				});


			});

		});
		$(document).ready(function(){
			$('.input-select').on('input',function(){
				if($('.input-select').val() == ''){
					$.get( "{{url('get/produk/search/all')}}", function( data ) {
						$( "#produk" ).html( data );
					
					});
				}else{
					$.get( "{{url('get/produk/search')}}/"+$('.input-select').val(), function( data ) {
						$( "#produk" ).html( data );
					
					});
				}
				
			});
		});
	 </script>
@endsection