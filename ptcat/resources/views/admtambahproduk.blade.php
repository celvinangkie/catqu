@extends('layouts.app')
@section('judul') 
<li><a href="home">Home</a></li> <!-- ini dan bawah diganti tiap halaman -->
<li><a href="admmasterbarang">Master Barang</a></li>
<li><a href="#">Tambah Produk</a></li>
@endsection
@section('content')
    <div class="row">

        <form class="form-horizontal" enctype="multipart/form-data" data-toggle="validator" method="POST" id="formtambahproduk" action="{{url('tambah/produk')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Barang</label>
                 <div class="col-md-6">
                    <input type="text" id="nama_produk" name="nama_produk" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Barang</label>
                 <div class="col-md-6">
                    <input type="number" id="price" name="price" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
            <div class="form-group">
               <label class="col-md-3 control-label">Warna Barang</label>
               <div class="col-md-2">
                     <select  id="warna" name="warna" >
                           <option value="">Pilih Warna</option>
                        @foreach ($warna as $warna)
                           <option value="{{$warna->warna}}-{{$warna->id}}">{{$warna->nama}}</option>
                        @endforeach
                        
                     </select>
                     
                     <span class="help-block with-errors"></span>
                  </div>
               <div class="col-md-2" >
                  <div class="col-md-1" id="divwarna" style="" >
                     <p></p>
                  </div>
               </div>
               
         </div>
         <div class="form-group">
            <label class="col-md-3 control-label">Kategori Barang</label>
            <div class="col-md-2">
                  <select  id="kategori" name="kategori" >
                        <option value="">Pilih Kategori</option>
                     @foreach ($kategori as $kategori)
                        <option value="{{$kategori->nama}}-{{$kategori->id}}">{{$kategori->nama}}</option>
                     @endforeach
                     
                  </select>
            </div>   
      </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Stok Barang</label>
                 <div class="col-md-6">
                    <input type="number" id="stok_masuk" name="stok_masuk" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
           
            <div class="form-group">
              <label class="col-md-3 control-label">Deskripsi Barang</label>
                 <div class="col-md-6">
                    <textarea rows = "10" cols = "78" id="deskripsi" name = "deskripsi"></textarea>
                 </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Upload Gambar Produk</label>
                <div class="col-md-6"><input id="prodimg" name="gambar" type="file" accept="image/*" name="prodimg"/>
                </div>
            </div><br>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                            
            </div>


        </form>
    </div>
@endsection
@section('script')
    <script>
       $(document).ready(function(){
         $("#warna").change(function(){
            if($('#warna').val()==''){
               $('#divwarna').attr('style','');
            }else{
               var warna = $('#warna').val().split("-");
               warna = warna[0];
               alert(warna);
               $('#divwarna').attr('style','border-style: solid; background-color:'+warna);
            }
         });
       });
       $(document).on('keypress',function(e) {
         if(e.which == 13) {
            var coba = "^^";
            // alert('tessss');
            // var txt = $.trim($(this).text());
            var box = $("#deskripsi");
            box.val(box.val() + coba);
         }
      });

    </script>
@endsection