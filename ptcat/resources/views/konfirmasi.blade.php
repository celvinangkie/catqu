@extends('layouts.app')
@section('content')
   <!-- BREADCRUMB -->
	<!-- SECTION -->
	<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
						<!-- Billing Details -->
						
					<div class="col-md-12 order-details">
						<div class="section-title text-center">
							<h3 class="title">Batas Bayar</h3>
						</div>
						<div class="section-title text-center">
							
							<h3 class="title">{{$duedate}}</h3>
						</div>
						<div class="section-title text-center">
							<h3 class="title">PESANAN ANDA</h3>
						</div>
						
						<div class="order-summary">
							<div class="order-col">
								<div><strong>PRODUK</strong></div>
								<div><strong>TOTAL</strong></div>
							</div>
							<div class="order-products">
								@foreach($nota_has_produk as $detailnota)
								<div class="order-col">
									<div>{{$detailnota->qty}}x {{$detailnota->namabarang}}</div>
									<div>Rp. {{$detailnota->subtotal}}</div>
								</div>
								@endforeach
							</div>
							<div class="order-col">
								<div><strong>TOTAL</strong></div>
								<div><strong class="order-total">Rp. {{$nota->grandtotal}}</strong></div>
							</div>
						</div>
					
				<!-- /row -->
					</div>

					<div class="shiping-details">
						<br>
						<br>
						<form class="form-horizontal" action="{{url('bayar')}}" enctype="multipart/form-data" data-toggle="validator" method="POST" id="form-konfirmasi-pembayaran">
							{{ csrf_field() }}
							<input type="hidden" name="idnota" value="{{$nota->idnota}}">
							<div class="section-title">
								<h4 class="title">Alamat Lengkap Pengiriman</h4>
							</div>
							<!-- Order notes -->
							<div class="order-notes">
								<textarea class="input" name="alamat" placeholder="Alamat"></textarea>
							</div>
							<div class="section-title">
								<h4 class="title">Catatan</h4>
							</div>
							<!-- Order notes -->
							<div class="order-notes">
								<textarea class="input" name="catatan" placeholder="Catatan"></textarea>
							</div>

							<div class="section-title">
								<h4 class="title">Bukti Pembayaran</h4>
							</div>
							<!-- Order notes -->
							<div class="order-notes">
								<input id="bukti" type="file" accept="image/*" class="" name="bukti"/>
							</div>
							<input type="submit" name="submit" class="primary-btn order-submit" value="Kirim Bukti Bayar">
						</form>
						<!-- /Order notes -->
					</div>
			
			<!-- /container -->
		</div>
	</div>
</div>
		<!-- /SECTION -->
			
@endsection