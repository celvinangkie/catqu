@extends('layouts.app')
@section('content')
    <style>
        table, th, td {border: 1px solid black; text-align: center;}
    </style>
    <div id="orderuser">
      @if($halaman == 'KonfirmasiPembayaran')
         <h3>Konfirmasi Pembayaran</h3>
         <table class="table">
         <tr>
            <th>Id Nota</th>
            <th>Tanggal</th>
            <th>Pembeli</th>
            <th>Total Harga</th>
            <th>Bukti Pembayaran</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
        @foreach($notabelis as $notabeli)
          <tr>
              <td>{{$notabeli->idnota}}</td>
              <td>{{$notabeli->tanggal}}</td>
              <td>{{$notabeli->name}}</td>
              <td>{{$notabeli->grandtotal}}</td> 
              <td> <img width="160px" height="120px" src="{{asset('storage/'.$notabeli->bukti_bayar)}}"></td>
              <td>{{$notabeli->alamat}}</td>
              <td>{{$notabeli->catatan}}</td>
              <td>{{$notabeli->status}}</td>
              <td><a href="{{url('setStatusNota/'.$notabeli->idnota.'/menunggu pengambilan')}}" class="btn btn-primary">Konfirmasi</a></td>
          </tr>
        @endforeach
      </table>
      @elseif($halaman == 'HistoryPembayaran')
          <h3>History Pembayaran</h3> 
          <table class="table">
          <tr>
            <th>Id Nota</th>
            <th>Tanggal</th>
            <th>Pembeli</th>
            <th>Total Harga</th>
            <th>Bukti Pembayaran</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>Status</th>
        </tr>
        @foreach($notabelis as $notabeli)
          <tr>
              <td>{{$notabeli->idnota}}</td>
              <td>{{$notabeli->tanggal}}</td>
              <td>{{$notabeli->name}}</td>
              <td>{{$notabeli->grandtotal}}</td> 
              <td> <img width="160px" height="120px" src="{{asset('storage/'.$notabeli->bukti_bayar)}}"></td>
              <td>{{$notabeli->alamat}}</td>
              <td>{{$notabeli->catatan}}</td>
              <td>{{$notabeli->status}}</td>
          </tr>
        @endforeach
      </table>
      @elseif($halaman == 'DataPenjualan')
          <h3>Data Penjualan</h3> 
          <table class="table">
          <tr>
            <th>Nama Produk</th>
            <th>Total yang Terjual</th>

        </tr>
        @foreach($penjualan as $penjualan)
          <tr>
              <td>{{$penjualan->namabarang}}</td>
              <td>{{$penjualan->total}}</td>
          </tr>
        @endforeach
      </table>
      @elseif($halaman == 'TerimaProduk')
          <h3>Terima Pembayaran</h3>
          <table class="table">
          <tr>
            <th>Id Nota</th>
            <th>Tanggal</th>
            <th>Pembeli</th>
            <th>Total Harga</th>
            <th>Bukti Pembayaran</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
        @foreach($notabelis as $notabeli)          
          <tr>
              <td>{{$notabeli->idnota}}</td>
              <td>{{$notabeli->tanggal}}</td>
              <td>{{$notabeli->name}}</td>
              <td>{{$notabeli->grandtotal}}</td> 
              <td> <img width="160px" height="120px" src="{{asset('storage/'.$notabeli->bukti_bayar)}}"></td>
              <td>{{$notabeli->alamat}}</td>
              <td>{{$notabeli->catatan}}</td>
              <td>{{$notabeli->status}}</td>
              <td><a href="{{url('setStatusNota/'.$notabeli->idnota.'/barang diambil')}}" class="btn btn-primary">Sudah Diambil</a></td>
          </tr>
        @endforeach
      </table> 
      @elseif($halaman == 'HistoryPembelian')
          <h3>History Pembayaran</h3>
          <table class="table">
          <tr>
            <th>Id Nota</th>
            <th>Tanggal</th>
            <th>Pembeli</th>
            <th>Total Harga</th>
            <th>Bukti Pembayaran</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>Status</th>
        </tr>
        @foreach($notabelis as $notabeli)
          <tr>
              <td>{{$notabeli->idnota}}</td>
              <td>{{$notabeli->tanggal}}</td>
              <td>{{$notabeli->name}}</td>
              <td>{{$notabeli->grandtotal}}</td> 
              <td> <img width="160px" height="120px" src="{{asset('storage/'.$notabeli->bukti_bayar)}}"></td>
              <td>{{$notabeli->alamat}}</td>
              <td>{{$notabeli->catatan}}</td>
              <td>{{$notabeli->status}}</td>
          </tr>
        @endforeach
         </table>
         @elseif($halaman == 'StatusPembayaran')
          <h3>Status Pembayaran</h3> 
          <table class="table">
          <tr>
            <th>Id Nota</th>
            <th>Tanggal</th>
            <th>Pembeli</th>
            <th>Total Harga</th>
            <th>Bukti Pembayaran</th>
            <th>Alamat</th>
            <th>Catatan</th>
            <th>Status</th>
        </tr>
        @foreach($notabelis as $notabeli)
          <tr>
              <td>{{$notabeli->idnota}}</td>
              <td>{{$notabeli->tanggal}}</td>
              <td>{{$notabeli->name}}</td>
              <td>{{$notabeli->grandtotal}}</td> 
              <td> <img width="160px" height="120px" src="{{asset('storage/'.$notabeli->bukti_bayar)}}"></td>
              <td>{{$notabeli->alamat}}</td>
              <td>{{$notabeli->catatan}}</td>
              <td>{{$notabeli->status}}</td>
          </tr>
        @endforeach
      </table>
      @endif

    </div>   
@endsection
@section('script')
    <script>
      
    </script>
@endsection