@extends('layouts.app')
@section('judul') 
<li><a href="/">Home</a></li> <!-- ini dan bawah diganti tiap halaman -->
<li>Detail Produk</li>
@endsection

@section('content')

<style type="text/css">
h3 {
    color: green;
}
</style>
    <div class="col-md-5 col-md-push-2">
                        <div id="product-main-img">
                            @foreach ($gambar as $gambar)    
                            <div class="product-preview">
                                <img src={{asset('storage/'.$gambar->gambar)}} alt="">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /Product main img -->

                    <!-- Product thumb imgs -->
                    <div class="col-md-2  col-md-pull-5">
                        <div id="product-imgs">
                            {{-- @foreach ($gambar as $gambar)    
                            <div class="product-preview">
                                <img src={{asset('storage/'.$gambar->gambar)}} alt="">
                            </div>
                            @endforeach --}}
                            
                        </div>
                    </div>
                    <!-- /Product thumb imgs -->

                    <!-- Product details -->

                    <div class="col-md-5">
                        <div class="product-details">
                            <h1 class="product-name">{{$produk->namabarang}}</h1> <h3>can i embracing the beautiful 
                                rainbow in my place? 
                                of course! choose our product</h3>
                        </div>
                            <div>
                                <h4 class="product-price">Harga: {{$produk->harga}}</h4>
                                <h4 class="product-available">Stok: {{$stok}}</h4>
                                <h4 class="product-available" >Warna: <h4 style="color:{{$warna->warna}}">{{$warna->nama}}</h4></h4>
                                
                            </div>
                            

                            <div class="add-to-cart">
                                <div class="qty-label">
                                    
                                    <div class="form-group" >
                                        <h4>Jumlah</h4>
                                        <input id="jumlah" type="number">
                                        
                                    </div>
                                </div><br><br>
                                <button onclick="tambahKeranjang()" class="primary-btn order-submit"> Tambah ke Keranjang</button><br><br>
                            <button onclick="beliproduk()"class="primary-btn order-submit"> Beli</button>  
                            <button onclick="requestOrder()"class="primary-btn order-submit"> Request Order</button>                               
                            
                        </div>
                           
                            {{-- <ul class="product-btns">
                                <li><a href="#"><i class="fa fa-heart-o"></i> add to wishlist</a></li>
                                <li><a href="#"><i class="fa fa-exchange"></i> add to compare</a></li>
                            </ul>

                            <ul class="product-links">
                                <li>Category:</li>
                                <li><a href="#">Headphones</a></li>
                                <li><a href="#">Accessories</a></li>
                            </ul>

                            <ul class="product-links">
                                <li>Share:</li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul> --}}
                            <br>
                            <div class="bg-primary text-white" id="pesan_req_order">
                                
                            </div>
                        </div>
                        
                    
                    <!-- /Product details -->

                    <!-- Product tab -->
                    <div class="col-md-12">
                        <div id="product-tab">
                            <!-- product tab nav -->
                            <ul class="tab-nav">
                                <li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
                                
                            </ul>
                            <!-- /product tab nav -->

                            <!-- product tab content -->
                            <div class="tab-content">
                                <!-- tab1  -->
                                <div id="tab1" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @foreach ($deskripsi as $item)
                                                <p>{{$item}}</p>                                                
                                            @endforeach
                                            {{-- <p>{{$produk->deskripsi}}</p> --}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /tab1  -->

                                <!-- tab2  -->
                                
                                        <!-- /Review Form -->
                                    </div>
                                </div>
                                <!-- /tab3  -->
                            </div>
                            <!-- /product tab content  -->
                        
                    <!-- /product tab -->
                    <!-- Section -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">

                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h3 class="title">Related Products</h3>
                        </div>.
                    </div>

                    <!-- product -->
                    @foreach ($produkrec as $produkrec)
                        <div class="col-md-3 col-xs-6">
                            <div class="product">
                                <div class="product-img" style="height:200px">
                                    <a href="/detailproduk/{{$produkrec->idproduk}}" class="cta-btn"><img src="{{asset('storage/'.$produkrec->gambar)}}" alt=""  style="height:200px"></a>
                                </div>
                                <div class="product-body">
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                    <!-- /product -->

                    <!-- /product -->

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
@endsection
@section('script')
<script>

    function requestOrder(){
        
        if(!$('#jumlah').val()){
            alert('jumlah produk yang di tidak boleh kosong')
        }else{
            $.ajax({
                url: "{{url('request/produk')}}",
                type : "POST",
                data:{
                    jumlah: $('#jumlah').val(), idproduk: "{{$produk->idproduk}}",
                    iduser:"{{Session::get('iduser')}}", _token: '{!! csrf_token() !!}',
                },
                success : function(data){

                    $('#pesan_req_order').html('<br>'+
                                    '<p style="margin-left: 20px"> Request sedang diproses</p>'+
                                    '<p style="margin-left: 20px"> Jika ada pertanyaan atau ingin membatalkan pesanan</p>'+
                                    '<p style="margin-left: 20px"> silahkan hubungan WA kami 081xxxxxxxx</p>'+
                                    // '<p style="margin-left: 20px"> Mohon transfer seharga Rp.'+data+' melalui no rek x</p>'+
                                    // '<p style="margin-left: 20px"> Kirim bukti pembayaran ke no WA x</p>'+
                                '<br>')
                },
                error : function(){
                    alert('error');
                }
            });   
        }
    }
    function tambahKeranjang(){
        
        if(!$('#jumlah').val()){
            alert('jumlah produk yang di tidak boleh kosong')
        }else{
            $.ajax({
                url: "{{url('tambah/keranjang')}}",
                type : "POST",
                data:{
                    jumlah: $('#jumlah').val(), idproduk: "{{$produk->idproduk}}",
                    iduser:"{{Session::get('iduser')}}", _token: '{!! csrf_token() !!}',
                },
                success : function(data){

                    alert(data);
                    location.reload();
                },
                error : function(){
                    alert('error');
                }
            });   
        }
    }
    function beliproduk(){
        if(!$('#jumlah').val()){
            alert('jumlah produk yang di tidak boleh kosong')
        }else{
            $.ajax({
                url: "{{url('beliproduk')}}",
                type : "POST",
                data:{
                    jumlah: $('#jumlah').val(), idproduk: "{{$produk->idproduk}}",
                     _token: '{!! csrf_token() !!}',
                },
                success : function(data){

                    alert(data);
                    location.reload();
                },
                error : function(){
                    alert('error');
                }
            });   
        }
    }
</script>
@endsection