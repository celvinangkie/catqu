@extends('layouts.app')
@section('judul') 
<li><a href="#">Home</a></li> <!-- ini dan bawah diganti tiap halaman -->
<li>Master Barang</li>
@endsection

@section('content')
    <style>
        table, th, td {border: 1px solid black; text-align: center;}
    </style>
      <!-- Modal -->
      <!-- Modal Edit -->
      <div class="modal fade" id="editbarang" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Produk</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal"  data-toggle="validator" method="POST" id="formtambahproduk" action="{{url('tambah/stok')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="idproduk" id="idproduk" value="">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Barang</label>
                 <div class="col-md-6">
                    <input type="text" id="nama_produk" name="nama_produk" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Barang</label>
                 <div class="col-md-6">
                    <input type="number" id="price" name="price" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
           
            <div class="form-group">
              <label class="col-md-3 control-label">Warna Barang</label>
                 <div class="col-md-6">
                    <input type="color" id="warna" name="warna">
                    <span class="help-block with-errors"></span>
                 </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Deskripsi Barang</label>
                 <div class="col-md-6">
                    <textarea rows = "10" cols = "40" id="deskripsi" name = "deskripsi"></textarea>
                 </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Upload Gambar Produk</label>
                <div class="col-md-6"><input id="prodimg" name="gambar" type="file" accept="image/*" name="prodimg"/>
                </div>
            </div><br>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>          
            </div>
        </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
      <!-- Modal Tambah Stok -->
      <div class="modal fade" id="tambahstok" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Tambah Stok</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal"  data-toggle="validator" method="POST" id="formtambahproduk" action="{{url('tambah/stok')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="idproduk" id="idprodukstok" value="">
            <div class="form-group">
              <label class="col-md-3 control-label">Stok</label>
                 <div class="col-md-6">
                    <input type="number" id="stok" name="stok" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div><br>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">Tambah Stok</button>
                </div>          
            </div>
        </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>

      <!-- Modal Tambah Kategori -->
      <div class="modal fade" id="myKategori" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Tambah Kategori</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal"  data-toggle="validator" method="POST" id="formtambahkategori" action="{{url('tambah/kategori')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">          
            <div class="form-group">
              <label class="col-md-3 control-label">Kategori</label>
                 <div class="col-md-6">
                    <input type="text" id="kategori" name="kategori" class="form-control">
                    <span class="help-block with-errors"></span>
                 </div>
            </div><br>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">Tambah Kategori</button>
                </div>          
            </div>
        </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
    <div>
        <p>Tambah Barang Baru</p>
        <a href="admtambahproduk" class="btn btn-primary" >Tambah Barang Baru</a><br><br><br>
    </div>
    <div>
      <p>Tambah Kategori</p>
      <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myKategori">Tambah Kategori</a><br><br><br>
    </div>
    <div>
        <p>Tambah Warna Baru</p>
        <a href="warna" class="btn btn-primary" >Tambah Warna Baru</a><br><br><br><br>
        <p>Daftar Barang Tersedia</p>
    </div>
    <div id="tabelproduk">
    
    </div>
@endsection
@section('script')

<script type="text/javascript">
$(function(){
    $.get("{{url('getmasterbarang')}}",function(data){
        $('#tabelproduk').html(data);            
    });

});

function editbarang(idproduk){
    $('#editbarang').modal('show');
    $('#idproduk').val(idproduk);
    // $.ajax({
    //         url: "{{url('get/produk')}}"+"/"+idproduk,
    //         type : "GET",
    //         dataType : "JSON",
    //         success : function(data){
    //     $('#nama_produk').val(data.namabarang);
    //     $('#price').val(data.harga);
    //     $('#warna').val(data.warna);
    //     $('#deskripsi').text(data.deskripsi);
    //         },
    //         error : function(){
    //             alert('maaf sistem tidak dapat menampilkan data');
    //         }
    //     });   
    
}
function tambahstok(idproduk){
  
    $('#tambahstok').modal('show');
    $('#idprodukstok').val(idproduk);
    
}
</script>
@endsection

