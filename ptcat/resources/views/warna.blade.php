@extends('layouts.app')
@section('judul') 
<li><a href="#">Home</a></li>  <!-- ini dan bawah diganti tiap halaman -->
<li>Master Warna</li>
@endsection
@section('content')
	<div class="container">
    <div class="row">
        <h3>Tambah Warna</h3>
        <form class="form-horizontal"  data-toggle="validator" method="POST" 
        id="formtambahproduk" action="{{url('/tambah/warna')}}">
        {{ csrf_field() }}
                  <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                       <div class="col-md-6">
                          <input type="text" id="nama" name="nama" class="form-control">
                          <span class="help-block with-errors"></span>
                       </div>
                  </div>
                                   
                  <div class="form-group">
                    <label class="col-md-3 control-label">Warna</label>
                       <div class="col-md-6">
                          <input type="color" id="warna" name="warna">
                          <span class="help-block with-errors"></span>
                       </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-6 col-md-offset-4">
                          <button type="submit" class="btn btn-primary">Tambah Warna</button>
                      </div>          
                  </div>
        </form>
            
    </div>
    <div class="row">
        <h3>Daftar Warna</h3>
        <table style="width:100%" class="table">
            <tr>
                <th>Nama Warna</th>
                <th>Warna</th>
            </tr>
            @foreach ($warna as $warna)
            <tr>
                <td>{{$warna->nama}}</td>
                <td>
                    <div class="col-md-1" style="background-color:{{$warna->warna}}">
                        <p>    </p>
                    </div>
                    </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection